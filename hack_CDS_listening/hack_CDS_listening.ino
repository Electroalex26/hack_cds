#include <SPI.h>
#include <NRFLite.h>

#define RADIO_ID        1
#define PIN_RADIO_CE    7
#define PIN_RADIO_CSN   8
#define chan_CDS        100

struct RadioPacket // Any packet up to 32 bytes can be sent.
{
  uint8_t FromRadioId;
  boolean portiqueOuvert;
  uint32_t FailedTxCount;
};

NRFLite radio;
RadioPacket radioData_CDS;

void setup()
{
  Serial.begin(115200);
  if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, chan_CDS))
  {
    Serial.println("Cannot communicate with radio");
    while (1);
  }
  radioData_CDS.portiqueOuvert = 0;
}

void loop()
{
  while (radio.hasData()) {
    radio.readData(&radioData_CDS);
    Serial.println("Portique : " + String(radioData_CDS.portiqueOuvert) + "    Millis : " + String(millis()/1000));
  }
}
