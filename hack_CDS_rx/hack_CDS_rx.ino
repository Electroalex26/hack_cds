#include <SPI.h>
#include <NRFLite.h>

#define RADIO_ID        0
#define RADIO_DEST_ID   1
#define PIN_RADIO_CE    7
#define PIN_RADIO_CSN   8
#define PIN_LED         LED_BUILTIN
#define chan_CDS        100
#define chan_Hack       80
#define TIME_CLOSE      10000
#define TIME_OPEN       1000

#define HACK_ON         48

struct RadioPacket // Any packet up to 32 bytes can be sent.
{
  uint8_t FromRadioId;
  boolean portiqueOuvert;
  uint32_t FailedTxCount;
};

NRFLite radio;
RadioPacket radioData_CDS;
uint8_t radioData_Hack;

unsigned long last;

void setup()
{
  Serial.begin(115200);
  pinMode(PIN_LED, OUTPUT);
  if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, chan_Hack))
  {
    Serial.println("Cannot communicate with radio");
    digitalWrite(PIN_LED, HIGH);
    while (1);
  }
  radioData_CDS.FromRadioId = RADIO_ID;
}

void loop()
{
  while (radio.hasData())
  {
    radio.readData(&radioData_Hack);
    Serial.println("Value : " + String(radioData_Hack));
    if (radioData_Hack == HACK_ON) {
      Serial.print("Hack ... ");
      // CLIGNOTE n fois
      for (int i = 0; i < 10; i++) {
        digitalWrite(PIN_LED, HIGH);
        delay(50);
        digitalWrite(PIN_LED, LOW);
        delay(50);
      }
      Serial.print("Changement de channel ... ");
      // CHANGEMENT DE CHANNEL
      if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, chan_CDS))
      {
        Serial.println("Cannot communicate with radio");
        digitalWrite(PIN_LED, HIGH);
        while (1);
      }
      Serial.print("Portique fermé ... ");
      // SPAM
      radioData_CDS.portiqueOuvert = 0;
      last = millis();
      while (millis() - last < TIME_CLOSE) {
        radio.send(RADIO_DEST_ID, &radioData_CDS, sizeof(radioData_CDS));
      }
      Serial.print("Portique ouvert ...");
      radioData_CDS.portiqueOuvert = 1;
      last = millis();
      while (millis() - last < TIME_OPEN) {
        radio.send(RADIO_DEST_ID, &radioData_CDS, sizeof(radioData_CDS));
      }
      Serial.print("Changement de channel ... ");
      // Remise sur le channel hack
      if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, chan_Hack))
      {
        Serial.println("Cannot communicate with radio");
        digitalWrite(PIN_LED, HIGH);
        while (1);
      }
      Serial.println("HACK COMPLETE !");
      // CLIGNOTE n fois
      for (int i = 0; i < 5; i++) {
        digitalWrite(PIN_LED, HIGH);
        delay(50*4);
        digitalWrite(PIN_LED, LOW);
        delay(50*4);
      }
    }
  }
}
