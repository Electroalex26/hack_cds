#include <SPI.h>
#include <NRFLite.h>

#define RADIO_ID        1
#define RADIO_DEST_ID   0
#define PIN_RADIO_CE    8
#define PIN_RADIO_CSN   7
#define chan_Hack       80

#define HACK_ON         48

NRFLite radio;
uint8_t radioData_Hack;

void setup()
{
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, chan_Hack))
  {
    Serial.println("Cannot communicate with radio");
    digitalWrite(LED_BUILTIN, HIGH);
    while (1);
  }
  radioData_Hack = HACK_ON;
}

void loop()
{
  Serial.print("Wait for beginning of hack ... ");
  Serial.println("Hack !");
  radio.send(RADIO_DEST_ID, &radioData_Hack, sizeof(radioData_Hack));
}
