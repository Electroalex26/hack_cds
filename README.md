# Hack chronomètre Coupe de Ski

Durant la coupe de ski organisée durant le SSE 2020, je me suis fixé comme objectif de foutre le bazard, voici ce qu'il en est ressorti ...
La documentation utilisée est la suivante : [sys_chrono_ski](https://git.elukerio.org/mablr/sys_chrono_ski)

## Principe

L'idée principale est de brouiller le signal du portique de départ. Pour cela nous allons utiliser un émetteur identique que nous allons placer au plus proche du récepteur CDS afin que notre signal soit bien plus fort que celui du portique.
Problématique : le concurrent doit pouvoir déclencher le système depuis le départ de la piste
Pour cela nous allons donc diviser le projet en 2 parties :
* Partie émetteur ayant pour seul but de déclencher la seconde partie dès qu'elle est mise sous tension par le concurrent
* Partie récepteur qui dès qu'elle est déclenchée va sur le canal CDS et simule un portique fermé pendant X secondes (durée durant laquelle le concurrent part) puis un portique ouvert

## Matériel

### Partie émetteur

* Arduino Nano
* NRF24L01 + LNA + PA
* Convertisseur buck 3.7->5V
* Accu 18650

### Partie récepteur

* Arduino Mega
* NRF24L01 + LNA + PA
* Powerbank

Afin de permettre une reprogrammation de cette partie directement sur les pistes, on y ajoute un RPI 3B+ que l'on pilotera en SSH/VNC depuis notre smartphone

## Fichiers

* hack_CDS_tx : Partie émetteur à téléverser sur l'Arduino Nano
* hack_CDS_rx : Partie récepteur à téléverser sur l'Arduino Mega
* hack_CDS_listening : Permet une écoute passive du portique, à téléverser dans l'Arduino Mega

## Verdict ?

10.42s !
